import { useDispatch, useSelector } from 'react-redux';

import {addNumber, executeAction} from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (number) => {
    const action = addNumber(number);
    dispatch(action);
  };
  const onClick = (action_type='DO_NOTHING') => {
    const action = executeAction(action_type);
    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClick('DOT')}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClick('SUM')}>+</button>
        <button onClick={() => onClick('SUBTRACT')}>-</button>
        <button onClick={() => onClick('MULTIPLY')}>x</button>
        <button onClick={() => onClick('DIV')}>/</button>
        <button onClick={() => onClick('SQR')}>√</button>
        <button onClick={() => onClick('SUM_ALL')}>Σ</button>
        <button onClick={() => onClick('UNDO')}>Undo</button>
        <button onClick={() => onClick('INTRO')}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
