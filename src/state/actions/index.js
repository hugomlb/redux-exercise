export const doNothing = () => ({
  type: 'DO_NOTHING',
});

export const addNumber =(number) => ({
  type: 'ADD_NUMBER_TO_DISPLAY',
  number: number,
});

export const executeAction =(action_type) => ({
  type: action_type,
});

export const operation = (minArgs, stackUpdater) => ({
  minArgs: minArgs,
  stackUpdater: stackUpdater,
})