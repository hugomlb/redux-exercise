export const selectCurrentNumber = (state) => {
  return state.current;
};

export const selectCurrentStack = (state) => {
  return state.stack;
};
